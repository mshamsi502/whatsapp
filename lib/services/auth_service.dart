import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:whatsapp/models/user.dart';


class AuthService {




  Future<User> sendDataToLogin(Map body) async {
    final response = await http.post(
//        'http://10.0.2.2/api/login',
        'http://10.1.2.94:8000/api/login',
        body: body
    );
    if (200 <= response.statusCode && response.statusCode <300) {
      Map responseBodyjson = json.decode(response.body);
      User user = User.fromJson(responseBodyjson);
      return user;
    } else {
      return null;
    }
  }

  static Future<bool> checkApiToken(String apiToken) async {
    final response = await http.get(
//        'http://10.0.2.2/api/user?api_token=$apiToken',
        'http://10.1.2.94:8000/api/user?api_token=$apiToken',
        headers: {'Accept': 'application/json'}
    );
    return response.statusCode == 200;

  }
}