

import 'package:sqflite/sqflite.dart';
import 'package:whatsapp/models/Product.dart';
import 'package:whatsapp/sqliteProvider/Provider.dart';

class ProductProvider extends Provider {
  String _tableName = 'product';

  Future<Product> insert(Product product, {conflictAlgorithm: ConflictAlgorithm.ignore}) async {
    await db.insert(_tableName, product.toMap(), conflictAlgorithm: conflictAlgorithm);
    return product;
  }

  Future<bool> insertAll(List<Product> products) async {
    await Future.wait(products.map((product) async {
          await this.insert(product);
    }
    ));
    return true;
  }

  Future<List<Product>> paginate(int page, {int limit: 4}) async {
    List<Map> maps = await db.query(_tableName,
    columns: ['id', 'user_id', 'title', 'body', 'image'],
      limit: limit,
      offset: page == 1 ? 0 : ((page -1) * limit)
    );
    if(maps.length > 0) {
      List<Product> products = [];
      maps.forEach((product) {
        if(product != null)
          products.add(Product.fromJson(product));
      });
      return products;
    }
    return null;
  }

}