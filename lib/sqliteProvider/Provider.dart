import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class Provider {
  Database db;
  String _path;

  Future open({String dbName:'my.db'}) async {

    var databasesPath = await getDatabasesPath();
    _path = join(databasesPath, dbName);

    db = await openDatabase(_path, version: 1,
        onCreate: (Database db, int version) async {
          await db.execute(
              'CREATE TABLE product (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER NOT NULL, title TEXT NOT NULL, body TEXT NOT NULL, image TEXT NOT NULL, price TEXT NOT NULL, created_at TEXT NOT NULL, updated_at TEXT NOT NULL)');
//          await db.execute('''
//            create table products (
//              id integer primary key autoincrement,
//              user_id integer not null,
//              title text not null,
//              body text not null,
//              image text not null,
//              price text not null,
//              created_at text not null,
//              updated_at text not null,
//          ''');
        });
  }

  Future close() async => db.close();
}
