import 'package:flutter/material.dart';

class CircleButtonWidget extends StatelessWidget {

  final GestureTapCallback onTap;
  final IconData icon;
  final double size;
  final Color color;
  final Color backgroundColor;
  const CircleButtonWidget({
    Key key,
    @required this.onTap,
    @required this.icon,
    this.size = 20,
    this.color =  Colors.black,
    this.backgroundColor = const Color(0x77ffffff),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      child: new Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: backgroundColor
        ),
        padding: const EdgeInsets.all(8),
        child: new Icon(
            icon,
            size: size,
            color: color
        )
      ),
      onTap: onTap
    );
  }
}