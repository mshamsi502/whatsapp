
import 'package:flutter/material.dart';


class InputFieldsArea extends StatelessWidget {
  final String hint;
  final bool obscure;
  final IconData icon;
  final validator;
  final onSaved;
  final defText;

  InputFieldsArea({this.hint, this.obscure, this.icon, this.validator, this.onSaved, this.defText});


  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: const EdgeInsets.only(bottom: 5),
      child: new TextFormField(
          validator: validator,
          initialValue: defText,
          onSaved: onSaved,
          obscureText: obscure,
          style: const TextStyle(
            color: Colors.white
          ),
        decoration: new InputDecoration(
          icon: new Icon(
              icon, color: Colors.white,
          ),
          focusedBorder: new UnderlineInputBorder(
              borderSide: new BorderSide(color: Colors.white)
          ),
          enabledBorder: new UnderlineInputBorder(
              borderSide: new BorderSide(color: Colors.white38)
          ),
          focusedErrorBorder: new UnderlineInputBorder(
              borderSide: new BorderSide(color: Colors.amber)
          ),
          errorStyle: new TextStyle(color: Colors.amber, fontWeight: FontWeight.w500),
          errorBorder: new UnderlineInputBorder(
            borderSide: new BorderSide(color: Colors.amber)
          ),
          hintText: hint,
          hintStyle: const TextStyle(color: Colors.white,  fontSize: 15),
          contentPadding: const EdgeInsets.only(
            top: 0,
            right: 0,
            bottom: 0,
            left: 5
          )
        ),
      ),
    );
  }

}