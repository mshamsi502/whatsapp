
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/models/Product.dart';
import 'package:whatsapp/pages/single_product_screen.dart';


class ProductCart extends StatelessWidget {
  final Product product;


  ProductCart({@required this.product});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var _screenSize = MediaQuery.of(context).size;
    return  new GestureDetector(
        onTap: () {
      Navigator.push(context, MaterialPageRoute(
          builder: (context) => SingleProductScreen(product: product,) ));
    },
    child:new Container(
      margin: const EdgeInsets.only(right: 5, left: 5, bottom: 10),
      child: new Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          new Container(
            height: 200,
            width: _screenSize.width,
            child: new CachedNetworkImage(
              //fit: BoxFit.cover,
//                imageUrl: product.image.replaceAll( 'http://www.auth.com/','http://10.0.2.2/'),
                imageUrl: product.image.replaceAll( 'http://www.auth.com/','http://10.1.2.94:8000/'),
              placeholder: (context, url) => new Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    new Opacity(
                      opacity: 0.3,
                      child: new Container(
                        width: _screenSize.width,
                        decoration: new BoxDecoration(
                            image: new DecorationImage(
                              fit: BoxFit.cover,
                                image: new AssetImage(
                                    'assets/images/loading_image.png'),
                            )
                        ),
                      ),
                    ),
                    new CircularProgressIndicator(
                      backgroundColor: Colors.black54,
                    ),
                  ],
                )
              ),
          ),
          new Container(
            alignment: Alignment.centerRight,
            height: 60,
            decoration: new BoxDecoration(
              color: Colors.black45,
            ),
            child: new Padding(
                padding: const EdgeInsets.only(right: 15, left: 5),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      product.title,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white),),
                    new RichText(
                      maxLines: 1,
                      text: TextSpan(
                          text: product.body,
                          style: TextStyle(
                              fontSize: 13,
                              color: Colors.white,
                              fontFamily: 'Vazir'
                          )
                      ),
                    ),
                  ],
                ),
            )
          )
        ],
      ),
    )
    );
  }
}
