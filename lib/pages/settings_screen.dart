import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/all_translations.dart';
import 'package:whatsapp/pages/select_language_screen.dart';

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(allTranslations.text("settings")),
      ),
      body: new ListView(
        children: <Widget>[
          new ListTile(
            leading: new Icon(Icons.language),
            title: new Text(allTranslations.text("choose_language")),
            onTap: () {
              Navigator.pushNamed(context, '/settings/select_language');
            },
          )
        ],
      )
    );
  }


}