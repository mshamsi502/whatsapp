import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/all_translations.dart';

class ContactUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    return Directionality(
        textDirection: TextDirection.rtl,
        child: new Scaffold(
          appBar: new AppBar(
            title: new Text(allTranslations.text("contact")),
          ),
          body: new Center(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(allTranslations.text("contact"), style: TextStyle(fontSize: 20)),
                  new Divider(
                    height: 10,
                  ),
                  new SizedBox(
                    height: deviceSize.height * 0.1,
                  ),
                  new Row(
                    children: <Widget>[
                      new Expanded(
                          child: Image.network(
                            'https://www.jeanswest.ir/b2b/assets/img/brand/avakatan.png',
                            fit: BoxFit.scaleDown,
                          )
                      )
                    ],
                  ),
                  new Text('اطلاعات تماس : 45183000-021'),
                  new Text('پست الکترونیکی : info@jeanswest.ir'),
                  new Text('ساعت کاری :'),
                  new Text('شنبه تا چهارشنبه : 17:00 - 09:00'),
                  new Text('پنجشنبه : 13:00 - 09:00'),
                  new SizedBox(
                    height: deviceSize.height * 0.1,
                  ),
                  new Divider(
                    height: 10,
                  ),
                  new RaisedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text(allTranslations.text("back")),
                  )
                ],
              )
          ),
        )
    );
  }


}