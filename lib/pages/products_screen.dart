import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/components/ProductCart.dart';
import 'package:whatsapp/models/Product.dart';
import 'package:whatsapp/services/product_service.dart';

class ProductsScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => new ProductsScreenState();
}

//with AutomaticKeepAliveClientMixin<ProductsScreen>
class ProductsScreenState extends State<ProductsScreen>  {
  List<Product> _products = [];
  int _currentPage = 1;
  bool _viewStram = true;
  bool _isLoading = true;
  ScrollController _listScrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    _getProducts();
    _listScrollController.addListener(() {
        double maxScroll = _listScrollController.position.maxScrollExtent;
        double currentScroll = _listScrollController.position.pixels;

        if(maxScroll - currentScroll <= 200)
            if(! _isLoading)
                _getProducts(page: _currentPage+1);
    });

  }

  _getProducts({ int page: 1, bool refresh: false }) async {
    setState(()=> _isLoading = true );
    var response = await ProductService.getProducts(page);
    setState(() {
      if(refresh) _products.clear();
      _products.addAll(response['products']);
      _currentPage = response['currentPage'];
      _isLoading = false;
    });
  }

  Widget loadingView() {
    return new Center(
      child: new CircularProgressIndicator(),
    );
  }
  Widget listIsEmpty() {
    return new Center(
      child: new Text('محصولی برای نمایش وجود نداره...'),
    );
  }
  Future<Null> _handleRefresh() async {
    await _getProducts(refresh: true);
    return null;
  }


  Widget StreamListView() {
    return _products.length == 0 && _isLoading
        ? loadingView()
        : _products.length == 0
            ? listIsEmpty()
            : new RefreshIndicator(
                child: new ListView.builder(
                    padding: const EdgeInsets.only(top: 0),
                    itemCount: _products.length,
                    itemBuilder: (BuildContext context, int index){
                      return new ProductCart(product: _products[index]);
                    }
                ),
                onRefresh: _handleRefresh,
    );
  }
  Widget ModuleListView() {
    return _products.length == 0 && _isLoading
        ? loadingView()
        : _products.length == 0
            ? listIsEmpty()
            : new RefreshIndicator(
                child: new GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                    padding: const EdgeInsets.only(top: 0),
                    itemCount: _products.length,
                    itemBuilder: (BuildContext context, int index){
                        return new ProductCart(product: _products[index]);
                    }
                ),
                onRefresh: _handleRefresh
              );

  }

  Widget headList() {
    return new SliverAppBar(
        primary: false,
        pinned: true,
        floating: true,
        elevation: 5,
        backgroundColor: Colors.transparent,
        //Color(0x22075e54),
        automaticallyImplyLeading: false,
        actions: <Widget>[
          new Padding(
            padding: const EdgeInsets.only(left: 10),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _viewStram = true;
                });
              },
              child: new Icon(
                  Icons.view_stream,
                  size: 30,
                  color: _viewStram
                      ? Color(0xff075e54)
                      : Colors.black
              ),
            ),
          ),
          new Padding(
            padding: const EdgeInsets.only(left: 10),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _viewStram = false;
                });
              },
              child: new Icon(
                  Icons.view_module,
                  size: 30,
                  color: _viewStram
                      ? Colors.grey[500]
                      : Color(0xff075e54)
              ),
            ),
          ),
        ],
    );
  }

  @override
  Widget build(BuildContext context) {
      return new NestedScrollView(
        controller: _listScrollController,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled){
            return _products.length != 0
                ? <Widget>[headList()]
                : [];
          },
          body: _viewStram
              ? StreamListView()
              : ModuleListView(),

      );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

}