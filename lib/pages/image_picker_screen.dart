import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';
import 'package:whatsapp/all_translations.dart';

class ImagePickerScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new ImagePickerScreenState();
}

class ImagePickerScreenState extends State<ImagePickerScreen> {
  Future<File> _imageFile;
  bool isVideo = false;
  VideoPlayerController _videoPlayerController;

  VoidCallback _listener;

  @override
  void initState() {
    super.initState();
    _listener = () {
      setState(() {});
    };
  }

  @override
  void dispose() {
    if (_videoPlayerController != null) {
      _videoPlayerController.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('اضافه کردن تصویر و ویدیو'),
        ),
        body: new NestedScrollView(
          reverse: true,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              new SliverAppBar(
                automaticallyImplyLeading: false ,
                actions: [
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      new SizedBox(width:
                          allTranslations.currentLanguage == 'en'
                              ? _screenSize.width * 0.18
                              : _screenSize.width * 0.16
                      ),
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              new Icon(
                                Icons.keyboard_arrow_up,
                                size: 20,
                              ),
                              new Text(
                                'گالری ',
                                style: TextStyle(fontSize: 11),
                              ),
                            ],
                          ),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              new Icon(
                                Icons.keyboard_arrow_down,
                                size: 20,
                              ),
                              new Text(
                                'دوربین',
                                style: TextStyle(fontSize: 11),
                              ),
                            ],
                          ),
                        ],
                      ),
                      new SizedBox(width: _screenSize.width * 0.03),
                      new Text(
                        'عکس',
                        style: TextStyle(fontSize: 20),
                      ),
                      new SizedBox(width: _screenSize.width * 0.03),
                      new VerticalDivider(
                        width: 10,
                        color: Colors.yellow,
                        indent: 10,
                        endIndent: 10,
                        thickness: 1,
                      ),
                      new SizedBox(width: _screenSize.width * 0.03),
                      new Text(
                        'ویدیو',
                        style: TextStyle(fontSize: 20),
                      ),
                      new SizedBox(width: _screenSize.width * 0.03),
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              new Icon(
                                Icons.keyboard_arrow_up,
                                size: 20,
                              ),
                              new Text(
                                'گالری ',
                                style: TextStyle(fontSize: 11),
                              ),
                            ],
                          ),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              new Icon(
                                Icons.keyboard_arrow_down,
                                size: 20,
                              ),
                              new Text(
                                'دوربین',
                                style: TextStyle(fontSize: 11),
                              ),
                            ],
                          ),
                        ],
                      ),
                      new SizedBox(width:
                      allTranslations.currentLanguage == 'en'
                          ? _screenSize.width * 0.19
                          : _screenSize.width * 0.17
                      ),
                    ],
                  )
                ],
              ),
            ];
          },
          body: new Center(child: isVideo ? perviewVideo() : _previewImage()),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: new Container(
          margin: const EdgeInsets.symmetric(horizontal: 5),
            padding: const EdgeInsets.symmetric(horizontal: 5 , vertical: 10),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    new Column(
                      children: [
                        new FloatingActionButton(
                          heroTag: 'pic_gallery' ,
                          onPressed: () {
                            isVideo = false;
                            _onImageButtonPressed(ImageSource.gallery);
                          },
                          child: Icon(Icons.photo_library, color: Colors.white),
                        ),
                        new SizedBox(
                          height: 10,
                        ),
                        new FloatingActionButton(
                            heroTag: 'pic_camera',
                          onPressed: () {
                            isVideo = false;
                            _onImageButtonPressed(ImageSource.camera);
                          },
                          child: Icon(Icons.camera_alt, color: Colors.white),
                        ),
                      ],
                    ),
                    new Column(
                      children: [
                        new FloatingActionButton(
                          heroTag: 'video_gallery',
                          backgroundColor: Colors.redAccent,
                          onPressed: () {
                            isVideo = true;
                            _onImageButtonPressed(ImageSource.gallery);
                          },
                          child: Icon(Icons.video_library, color: Colors.white),
                        ),
                        new SizedBox(
                          height: 10,
                        ),
                        new FloatingActionButton(
                          heroTag: 'video_camera',
                          backgroundColor: Colors.redAccent,
                          onPressed: () {
                            isVideo = true;
                            _onImageButtonPressed(ImageSource.camera);
                          },
                          child: Icon(Icons.videocam, color: Colors.white),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            )));
  }

  void _onImageButtonPressed(ImageSource source) {
    if (_videoPlayerController != null) {
      _videoPlayerController.setVolume(0);
      _videoPlayerController.removeListener(_listener);
    }
    if (isVideo) {
      _imageFile = ImagePicker.pickVideo(source: source).then((File file) {
        if (file != null) {
          _videoPlayerController = VideoPlayerController.file(file)
            ..addListener(_listener)
            ..setVolume(1)
            ..initialize()
            ..setLooping(true)
            ..play();
        }
      });
    } else {
      _imageFile = ImagePicker.pickImage(source: source);
    }
    if (mounted) setState(() {});
  }

  perviewVideo() {
    if (_videoPlayerController == null) {
      return new Text('هنوز ویدیویی انتخاب نکردی!');
    } else if (_videoPlayerController.value.initialized) {
      return AspectRatio(
        aspectRatio: 1,
        child: VideoPlayer(_videoPlayerController),
      );
    } else {
      return new Text(
        'ویدیوت لود نشد!',
        textAlign: TextAlign.center,
      );
    }
  }

  Widget _previewImage() {
    return FutureBuilder<File>(
      future: _imageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return Image.file(snapshot.data);
        } else if (snapshot.error != null) {
          return new Text(
            'تصویرت لود نشد!',
            textAlign: TextAlign.center,
          );
        } else {
          return new Text('هنوز تصویری انتخاب نکردی!');
        }
      },
    );
  }
}
