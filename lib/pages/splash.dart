
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:whatsapp/Helper.dart';
import 'package:whatsapp/all_translations.dart';
import 'package:whatsapp/services/auth_service.dart';
import 'package:whatsapp/translation.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new SplashScreenState();

}

class SplashScreenState extends State<SplashScreen> {
  final _scaffoldkey = GlobalKey<ScaffoldState>();


  @override
  void initState() {
    super.initState();
    checkLogin();

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldkey,
      backgroundColor: new Color(0xff075e54),
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                width: 125,
                height: 125,
                decoration: new BoxDecoration(
                  image: new DecorationImage(image: new AssetImage('assets/images/whatsapp_logo.png')
                  ),
                ),

              ),
              new Text(
                allTranslations.text('title'),
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.bold
                ),
              )

            ],
          ),
          new Padding(
              padding: const EdgeInsets.only(bottom: 80),
            child:  new Align(
                alignment: Alignment.bottomCenter,
                child: new CircularProgressIndicator(),
            ),
          ),
        ],
      ),
    );
  }

  void checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('user.api_token');

    if(await checkConnectionInternet()) {
      //check api token
      if(apiToken == null) navigationPageToLogin();
      await AuthService.checkApiToken(apiToken)
          ? navigationPageToHome()
          : navigationPageToLogin();


    } else {
      _scaffoldkey.currentState.showSnackBar(
          new SnackBar(
            duration: new Duration(hours: 2),
              content: new GestureDetector(
                onTap: () {
                  _scaffoldkey.currentState.hideCurrentSnackBar();
                  checkLogin();
                },
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Icon(Icons.touch_app, color: Colors.amber,),
                    new Row(
                      children: <Widget>[
                        new Icon(Icons.wifi_lock, color: Colors.white,),
                        new SizedBox(
                          width: 20,
                        ),
                        new Text(
                          'داداش اینترنتتو یه چک بکن ;)',
                          style: TextStyle(fontFamily: 'Vazir', color: Colors.white),
                        ),
                      ],
                    ),
                    new Icon(Icons.touch_app, color: Colors.amber,),
                  ],
                ),
              )
          )
      );
    }
  }


  navigationPageToLogin() {
    Navigator.of(context).pushReplacementNamed('/login');
  }
  navigationPageToHome() {
    Navigator.of(context).pushReplacementNamed('/home');
  }


}