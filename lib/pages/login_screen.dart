import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:whatsapp/all_translations.dart';
import 'package:whatsapp/animations/signin_animation.dart';
import 'package:whatsapp/components/Form.dart';
import 'package:whatsapp/models/user.dart';
import 'package:whatsapp/pages/select_language_screen.dart';
import 'package:whatsapp/services/auth_service.dart';
import 'package:whatsapp/translation.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _loginButtonController;
  final _formKey = GlobalKey<FormState>();
  final _scaffolfKey = GlobalKey<ScaffoldState>();
  String _emailValue;
  String _passwordValue;

  emailOnSaved(String value) {
    _emailValue = value;
  }

  passwoedOnSaved(String value) {
    _passwordValue = value;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loginButtonController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 2000));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _loginButtonController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 0.5;
    var deviceSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: _scaffolfKey,
      //resizeToAvoidBottomPadding: false, //false: fix position with keyboard
      body: new Container(
        decoration: new BoxDecoration(
            gradient: new LinearGradient(
          colors: <Color>[
            const Color(0xff1fbda4),
            const Color(0xff0e1e24),
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        )),
        child: new Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            new Opacity(
              opacity: 0.2,
              child: new Container(
                width: deviceSize.width,
                height: deviceSize.height,
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                        image: new AssetImage(
                            'assets/images/login_background.png'),
                        repeat: ImageRepeat.repeat)),
              ),
            ),
            new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new FormContainer(
                  formKey: _formKey,
                  emailOnSaved: emailOnSaved,
                  passwordOnSaved: passwoedOnSaved,
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Text(
                      allTranslations.text('register_text'),
                      style: TextStyle(
                          fontWeight: FontWeight.w300,
                          letterSpacing: 0.5,
                          color: Colors.white,
                          fontSize: 16),
                    ),
                    new FlatButton(
                      onPressed: null,
                      child: new Text(
                        allTranslations.text('register_button'),
                        style: TextStyle(
                          fontWeight: FontWeight.w300,
                          letterSpacing: 0.5,
                          color: Colors.white,
                          fontSize: 16,
                          shadows: <Shadow>[
                            Shadow(
                                offset: Offset(2.0, 2.0),
                                blurRadius: 0.0,
                                color: Colors.black),
                            Shadow(
                                offset: Offset(3.0, 3.0),
                                blurRadius: 5.0,
                                color: Colors.amberAccent),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                new SizedBox(
                  height: 80,
                ),
                new Divider(
                  indent: 80,
                  endIndent: 80,
                  thickness: 0.7,
                  color: Color(0xff9f9f00),
                  height: 0,
                ),
                new FlatButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/settings/select_language');
                    },
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        new SizedBox(
                          width: 10,
                        ),
                        new Text(
                          'انتخاب زبان',
                          style: TextStyle(
                            fontWeight: FontWeight.w200,
                            letterSpacing: 0.5,
                            color: Colors.white,
                            fontSize: 11,
                            shadows: <Shadow>[
                              Shadow(
                                  offset: Offset(2.0, 2.0),
                                  blurRadius: 0.0,
                                  color: Colors.black),
                              Shadow(
                                  offset: Offset(3.0, 3.0),
                                  blurRadius: 5.0,
                                  color: Colors.amberAccent),
                            ],
                          ),
                        ), //انتخاب زبان
                        new SizedBox(
                          width: 20,
                        ),
                        new Icon(
                          Icons.language,
                          color: Color(0xff9f9f00),
                          size: 18,
                        ),
                        new SizedBox(
                          width: 10,
                        ),
                        new Text(
                          'Choose Language',
                          style: TextStyle(
                            fontWeight: FontWeight.w200,
                            letterSpacing: 0.5,
                            color: Colors.white,
                            fontSize: 11,
                            shadows: <Shadow>[
                              Shadow(
                                  offset: Offset(2.0, 2.0),
                                  blurRadius: 0.0,
                                  color: Colors.black),
                              Shadow(
                                  offset: Offset(3.0, 3.0),
                                  blurRadius: 5.0,
                                  color: Colors.amberAccent),
                            ],
                          ),
                        ), //choose lang
                        new SizedBox(
                          width: 10,
                        ),
                      ],
                    ))
              ],
            ),
            new GestureDetector(
              onTap: () async {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  sendDataForLogin();
                }
              },
              child: new SignInAnimation(
                controller: _loginButtonController.view,
              ),
            )
          ],
        ),
      ),
    );
  }

  sendDataForLogin() async {
    await _loginButtonController.animateTo(0.150);
    User response = await (new AuthService())
        .sendDataToLogin({"email": _emailValue, "password": _passwordValue});
    if (response != null) {
      await storeUserData(response);
      await _loginButtonController.forward();
      Navigator.pushReplacementNamed(context, '/home');
    } else {
      await _loginButtonController.reverse();
      _scaffolfKey.currentState.showSnackBar(new SnackBar(
        content: new Text(
          'مشتی اشتباه زدی! با دقت بیشتری وارد کن :))',
          style: new TextStyle(fontFamily: 'Vazir'),
        ),
      ));
    }
  }

  storeUserData(User userdata) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('user.api_token', userdata.api_token);
    await prefs.setInt('user.id', userdata.id);
  }
}
