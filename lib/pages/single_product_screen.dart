import 'package:bubble/bubble.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:whatsapp/models/Product.dart';

class SingleProductScreen extends StatefulWidget {
  final Product product;

  SingleProductScreen({ @required this.product});

  @override
  State<StatefulWidget> createState() =>
      SingleProductScreenState(product: this.product);
}

class SingleProductScreenState extends State<SingleProductScreen> {
  final Product product;

  SingleProductScreenState({ @required this.product});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery
        .of(context)
        .size;
    return new Directionality(
      textDirection: TextDirection.rtl,
      child: new Scaffold(
        appBar: new AppBar(
          title: new Text(product.title),
        ),
        body: new Stack(
          children: [
            new Padding(
              padding: const EdgeInsets.all(10),
              child: new Container(
                height: 100,
                decoration: new BoxDecoration(
                    gradient: new LinearGradient(
                        colors: <Color>[
                          const Color(0xffdcf8c6),
                          const Color(0x00ece5dd),
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter
                    )
                ),
                child:
              new Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  new SizedBox(width: 1,),
                  new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      new Text(
                        product.title,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      new Text(
                        'قیمت : ${product.price} تومان',
                        style: TextStyle(
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ],
                  ),
                  new SizedBox(width: 5,),
                  new FlatButton(
                      color: Colors.green,
                      onPressed: () async {

                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        String apiToken = prefs.getString('user.api_token');
                        String url = 'http://10.1.2.94:8000/pay';
//                        String url = 'http://rookcket.org/api/product/buy?product_id=1&api_token=${apiToken}';



                        if (await canLaunch(url)) {
                            await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }

                      },
                      child: new Text(
                        'خرید', style: TextStyle(color: Colors.white),)
                  ),
                  new SizedBox(width: 1,),
                ],
              ),
              )
            ),
            new Padding(
                padding: const EdgeInsets.only(right: 10, left: 10, bottom: 10),
                child: new DraggableScrollableSheet(
                    initialChildSize: 0.8,
                    minChildSize: 0.7,
                    maxChildSize: 0.85,
                    builder: (context, scrollController) =>
                        Container(
                            decoration: new BoxDecoration(
                                gradient: new LinearGradient(
                                    colors: <Color>[
                                      const Color(0x00ece5dd),
                                      const Color(0xffdcf8c6),
                                    ],
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter
                                ),
                            ),
                            child: SingleChildScrollView(
                            controller: scrollController,
                            child: new Container(
                              decoration: new BoxDecoration(
                                  gradient: new LinearGradient(
                                      colors: <Color>[
                                        const Color(0x00ece5dd),
                                        const Color(0xffdcf8c6),
                                      ],
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter
                                  ),
                              ),
                                child: new Column(
                                  children: [
                                new Padding(
                                padding: const EdgeInsets.all(5),
                                child: new Container(
                                  color: Colors.greenAccent,
                                  child: new CachedNetworkImage(
                                      width: _screenSize.width,
                                      height: 200,
                                      imageUrl: product.image.replaceAll( 'http://www.auth.com/','http://10.1.2.94:8000/'),
                                      fit: BoxFit.cover,
                                      ),
                                    ),
                                    ),
                                    new Padding(
                                        padding: const EdgeInsets.all(5),
                                        child: new Container(
                                          decoration: new BoxDecoration(
                                            gradient: new LinearGradient(
                                                colors: <Color>[
                                                  const Color(0x00ece5dd),
                                                  const Color(0xffdcf8c6),
                                                ],
                                                begin: Alignment.topCenter,
                                                end: Alignment.bottomCenter
                                            ),
                                          ),
                                          child: new Text(product.body),
                                        )

                                    ),

                                  ],
                                )

                            )
                        )
                        )
                )

            ),
          ],
        ),

      ),
    );
  }


}