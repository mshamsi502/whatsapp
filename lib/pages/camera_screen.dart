import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'package:thumbnails/thumbnails.dart';
import 'package:whatsapp/all_translations.dart';
import 'package:whatsapp/pages/view_file_screen.dart';


class CameraScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new CameraScreenState();
}

class CameraScreenState extends State<CameraScreen> {
  CameraController _cameraController;
  List<CameraDescription> _cameras;
  CameraDescription _cameraDescription;
  String tempFilePath;


  List<Map> _files = [];

  final GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    initCamera();
  }

  @override
  void dispose() {
    _cameraController.dispose();
    super.dispose();
  }

  initCamera() async {
    _cameras = await availableCameras();
    selectCamera(camera: _cameras[0]);
    _cameraDescription = _cameras[0];
  }
  selectCamera({CameraDescription camera}) async {
    setState(() {
      _cameraController = CameraController(camera, ResolutionPreset.high);
      _cameraDescription = camera;

    });
    _cameraController.addListener(() {
      if(_cameraController.value.hasError) {
          _showInSnackBar(
              allTranslations.text("error_camera") + "\n${_cameraController.value.errorDescription}"
          );
      }
    });

    try {
        await _cameraController.initialize();
    } on CameraException catch(e) {
      _showCameraException(e);
    }
    if(mounted) setState(() {});

  }
  void _showInSnackBar(String message) {
    _scaffoldkey.currentState.showSnackBar(
      SnackBar(
        content: new Text(message,
          textDirection: TextDirection.rtl,
          style: TextStyle(
            fontFamily: 'Vazir'
          ),
        ),
      )
    );
  }
  void _showCameraException(CameraException e) {
    print('Error: ${e.code}\nError Message: ${e.description}');
    _showInSnackBar(allTranslations.text("error_message") + '\n${e.description}');
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(child: new Scaffold(
      key: _scaffoldkey,
      body: new Stack(
          children: <Widget>[
          _cameraPreviewWidget(),
          _cameraButtomSection(context)
        ],
      ),
    ),
    );
  }

  Widget _cameraPreviewWidget() {
    if(_cameraController == null || ! _cameraController.value.isInitialized) {
        return new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new CircularProgressIndicator(),
              new SizedBox(
                height: 30,
              ),
              new Text(
                allTranslations.text("find_camera"),
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ],
          )
        );
      }
    else {
      return new Expanded(child: new Transform.scale(
        scale: 1,
        child: Center(
          child: AspectRatio(
            aspectRatio: _cameraController.value.aspectRatio,
            child: new CameraPreview(_cameraController),
          ),
        ),
      ),
      );
    }
  }
  Widget _cameraButtomSection(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    return new Align(
      alignment: Alignment.bottomCenter,
      child: new Padding(
          padding: EdgeInsets.only(bottom: 25),
          child : new Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              _files.length != 0 // thumbnails pic and video
                ? new Padding (
                  padding: const EdgeInsets.only(bottom: 10),
                  child: new SizedBox(
                    height: 60,
                    width: _screenSize.width,
                    child: new Container(
                      color: Color(0x3212a8a8),
                      child: new ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: _files.length,
                          reverse: true,
                          itemBuilder: (BuildContext context, int index) {
                            Map file = _files[index];
                            String type = file['type'];
                            String imagePath = file['type']  == 'image'
                                ? file['path']
                                : file['thumb'];
                             return new GestureDetector(
                               onTap: (){
                                 Navigator.push(context, MaterialPageRoute(builder: (context) => ViewFileScreen(file: file,) ));
                               },
                                child: new Padding(
                                  padding: const EdgeInsets.only(left: 7, top: 7, bottom: 7),
                                  child: new SizedBox(
                                    width: 60,
                                    child: new Stack(
                                      fit: StackFit.expand,
                                      children: [
                                        Image.file(File(imagePath), fit: BoxFit.cover),
                                        type == 'video'
                                            ? new Align(
                                          alignment: Alignment.bottomLeft,
                                          child: new Padding(
                                            padding: const EdgeInsets.only(left: 2, bottom: 2),
                                            child: Icon(
                                                Icons.camera_alt,
                                                size: 18,
                                                color: Colors.amberAccent
                                            ),
                                          ),
                                        )
                                            : type == 'image'
                                                ? new Align(
                                                  alignment: Alignment.bottomLeft,
                                                  child: new Padding(
                                                      padding: const EdgeInsets.only(left: 2, bottom: 2),
                                                      child: Icon(
                                                          Icons.image,
                                                          size: 18,
                                                          color: Colors.greenAccent
                                                      ),
                                                  ),
                                               )
                                            : new SizedBox()
                                      ],
                                    ),
                                  ),
                                )
                            )
                            ;
                          }
                      ),
                    )
                )
              )
                : new SizedBox()
              ,
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  new IconButton(
                      icon: new Icon(
                          Icons.switch_camera,
                        size: 36,
                        color: Colors.white,
                      ),
                      onPressed: _cameraSwitchToggle
                  ),
                  new GestureDetector(
                    onTap:_onTakePictueButtonPressed,
                    onLongPress: _onStartVideoRecording,
                    onLongPressUp: _onStopVideoRecording,
                    child: new Container(
                      width: 65,
                      height: 65,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        image: DecorationImage(
                          image: new AssetImage('assets/images/tap.png'),
                        ),
                      border: new Border.all(
                          width: 4,
                          color: _cameraController != null && _cameraController.value.isRecordingVideo
                            ? Colors.redAccent
                            : Colors.white
                        ),
                        shape: BoxShape.circle
                      ),
                    ),
                  ),
                  new IconButton(
                      icon: new Icon(
                        Icons.flash_off,
                        size: 36,
                        color: Colors.white,
                      ),
                      onPressed: () {}
                  ),
                ],
              ), // button row
              new Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: new Text(
                    allTranslations.text("help_camera"),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontFamily: 'Nazanin'
                    ),
                  ),
              ),  //help text
            ],
          )
      ),
    );
  }


  void _cameraSwitchToggle() {
    if(_cameras.length >=2)
      selectCamera(
        camera: _cameraDescription == _cameras[0]
            ? _cameras[1]
            : _cameras[0]
      );
    else {
      _showInSnackBar(allTranslations.text("cant_change_camera"));
    }
  }

  void _onTakePictueButtonPressed() async {
      String filePath = await takePicure();
//       create try catch
      setState(() {
        _files.add({
          'type': 'image',
          'path' : filePath
        });
      });
      //if(filePath != null)
    _showInSnackBar(allTranslations.text("save_picture") + '\n$filePath');
    print(filePath);
  }
  Future<String> takePicure() async {
    //final Directory extDir = await getApplicationDocumentsDirectory();
    final Directory extDir = await getExternalStorageDirectory();
    final String dirPath = '${extDir.path}/whatsapp/Pictures';
    //final String dirPath = '${extDir.path}/whatsapp/Media';
    await Directory(dirPath).create(recursive: true);
    final filePath = '$dirPath/${timestamp()}.jpg';
    try {
      await _cameraController.takePicture(filePath);
    } on CameraException catch(e) {
      _showCameraException(e);
      return null;
    }
    return filePath;
  }
  timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  Future<void> _onStartVideoRecording() async {
    if(_cameraController.value.isRecordingVideo) {
      _showInSnackBar(allTranslations.text("recording_video"));
      return;
    }

//    final Directory extDir = await getApplicationDocumentsDirectory();
    //save in : data/data//com.example.yourapp/app_flutter/Picture/whatsapp/

    final Directory extDir = await getExternalStorageDirectory();
    //save in : sdcard/Android/data/com.example.yourapp/files/Picture/whatsapp/

    final String dirPath = '${extDir.path}/whatsapp/Videos';
    //final String dirPath = '${extDir.path}/whatsapp/Media';
    await Directory(dirPath).create(recursive: true);
    final filePath = '$dirPath/${timestamp()}.mp4';

    try {
      await _cameraController.startVideoRecording(filePath);
    } on CameraException catch(e) {
      _showCameraException(e);
    }
    setState(() {
      tempFilePath = filePath;
    });
  }
  void _onStopVideoRecording() async{
    if(! _cameraController.value.isRecordingVideo) {
      _showInSnackBar(allTranslations.text("not_recording_video"));
      return;
    }


    try {
      await _cameraController.stopVideoRecording();
    } on CameraException catch(e) {
      _showCameraException(e);
    }

    if(tempFilePath == null) {
      _showInSnackBar(allTranslations.text("not_found_record"));
    }

    final Directory tempDir = await getTemporaryDirectory();
    //save in : data/data//com.example.yourapp/app_flutter/Picture/whatsapp/

    String thumb = await Thumbnails.getThumbnail(
        thumbnailFolder:'${tempDir.path}/whatsapp/Videos', // creates the specified path if it doesnt exist
        videoFile: tempFilePath,
        imageType: ThumbFormat.JPEG,
        quality: 60);

    setState(() {
      _files.add({
        'type' : 'video',
        'thumb' : thumb,
        'path' : tempFilePath,

      });
      _showInSnackBar(allTranslations.text("save_video") + '\n${tempFilePath}');
      print(tempFilePath);
      tempFilePath = null;
    });
  }

}




