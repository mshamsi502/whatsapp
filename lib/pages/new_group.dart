import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/all_translations.dart';

class NewGroup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: new Scaffold(
          appBar: new AppBar(
            title: new Text(allTranslations.text("new_group")),
          ),
          body: new Center(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(allTranslations.text("new_group"), style: TextStyle(fontSize: 20)),
                  new RaisedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text(allTranslations.text("back")),
                  )
                ],
              )
          ),
        )
    );
  }


}