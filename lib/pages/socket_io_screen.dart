import 'dart:convert';
import 'dart:math';

import 'package:bubble/bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_socket_io/flutter_socket_io.dart';
import 'package:flutter_socket_io/socket_io_manager.dart';
import 'package:whatsapp/all_translations.dart';
import 'package:whatsapp/models/chat_model.dart';

import 'image_picker_screen.dart';

class SocketIoScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SocketIoScreenState();

}

class SocketIoScreenState extends State<SocketIoScreen> {

  TextEditingController _textEditingController = new TextEditingController();
  List<ChatModel> _messages = [];
  int _userId;
  SocketIO _socketIO;

  @override
  void initState() {
    super.initState();
    _userId = new Random().nextInt(1000);

    _socketIO = SocketIOManager().createSocketIO(
        "http://10.1.2.94:3000",
        '/',
        socketStatusCallback: _socketStatus
    );
    _socketIO.init();
    _socketIO.subscribe('messages', _onReceiveNewMessage);
    _socketIO.connect();

  }
  @override
  void dispose() {
    if(_socketIO != null) {
      _socketIO.destroy();
    }
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(allTranslations.text("chat")),
      ),
      body: new Stack(
        children: [
          new Expanded(
              child: new Padding(
                  padding: const EdgeInsets.only(right: 10, left: 10, top: 10),
                child: new ListView.builder(
                    itemCount: _messages.length+1,
                    itemBuilder: (BuildContext context, int index) {
                      return new Container(
                      child: index == 0
                          ? new Bubble(
                            alignment: Alignment.center,
//                            color: Color.fromRGBO(212, 234, 244, 1.0),
                            color: Color(0xfffff6ca),
                            child: Text(
                                allTranslations.text("today"),
                                textAlign: TextAlign.center, style: TextStyle(fontSize: 11.0)),
                          )
                      : _userId == _messages[index-1].id
                        ? new Bubble(
                          margin: BubbleEdges.only(top: 10),
                          alignment: Alignment.topRight,
                          nip: BubbleNip.rightTop,
                          color: Color.fromRGBO(225, 255, 199, 1.0),
                          child: Text(_messages[index-1].message, textAlign: TextAlign.right),
                        )
                            : new Bubble(
                          margin: BubbleEdges.only(top: 10),
                          alignment: Alignment.topLeft,
                          nip: BubbleNip.leftTop,
                          color: Color(0xffbaf2f3),
                          child: Text(_messages[index-1].message),
                        ),
                      );
                    }
                ),
              )
          ),
          new Container(
            decoration: BoxDecoration(
              color: Colors.black12
            ),
          ),
          new Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
                     new Container(
                       margin: const EdgeInsets.only(right: 5, left: 5, bottom: 5),
                       decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(100),
                         color: Colors.white
                       ),
                      child: new Row(
                        children: [
                                new IconButton(
                                    icon: new Icon(Icons.insert_emoticon, color: Colors.blueGrey,),
                                    onPressed: () {}
                                ),
                                new Expanded(
                                    child: new TextField(
                                      controller: _textEditingController,
                                      decoration: new InputDecoration(
                                          hintText: allTranslations.text("type"),
                                          border: InputBorder.none
                                      ),
                                    )
                                ),
                                new IconButton(
                                    icon: new Icon(Icons.attach_file, color: Colors.blueGrey,),
                                    onPressed: () {}
                                ),
                                new IconButton(
                                    icon: new Icon(Icons.camera_alt, color: Colors.grey[800],),
                                    onPressed: () {
                                      Navigator.of(context).pushNamed('/image_picker');
                                    }
                                ),
                                new Container(
                                  margin: const EdgeInsets.all(2),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    color: Color(0xff075e54),
                                  ),
                                  child:  new IconButton(
                                      icon: new Icon(Icons.send,color: Colors.white,),
                                      onPressed: () {
                                        String msg = _textEditingController.text;
                                        if(_textEditingController.text.length > 0)
                                          setState(() {
                                            _sendChatMassege(msg);
                                            _messages.add(
                                                ChatModel(
                                                    id: _userId,
                                                    message: msg
                                                )
                                            );
                                            _textEditingController.text = '';
                                          });
                                      }
                                  ),
                                ) ,
                        ],
                      ),
              ),
            ],
          )
        ]
    )
    );
  }



  _socketStatus(dynamic data) {
    print('Socket Status : ' + data);
  }

  void _sendChatMassege(String msg) {
    if(_socketIO != null) {
      Map<String , dynamic> jsonData = {
        'id' : _userId,
        'message' : msg
      };
      
      _socketIO.sendMessage('send_message', json.encode(jsonData));


    }
  }

  _onReceiveNewMessage(dynamic message) {
    Map<String, dynamic> msg =json.decode(message);
    print(msg);
    setState(() {
      _messages.add(
        new ChatModel(
          id: msg['id'],
          message: msg['message']
        )
      );
    });
  }


}
