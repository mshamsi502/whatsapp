import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/all_translations.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    return Directionality(
        textDirection: TextDirection.rtl,
        child: new Scaffold(
          appBar: new AppBar(
            title: new Text(allTranslations.text("about")),
          ),
          body: new Center(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(allTranslations.text("about"), style: TextStyle(fontSize: 20)),
                  new Divider(
                    height: 10,
                  ),
                  new SizedBox(
                    height: deviceSize.height * 0.2,
                  ),
                  new Row(
                    children: <Widget>[
                      new Expanded(
                          child: Image.network(
                            'https://www.jeanswest.ir/b2b/assets/img/brand/avakatan.png',
                            fit: BoxFit.scaleDown,
                          )
                      )
                    ],
                  ),
                  new SizedBox(
                    height: deviceSize.height * 0.2,
                  ),
                  new Divider(
                    height: 10,
                  ),
                  new RaisedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text(allTranslations.text("back")),
                  )
                ],
              )
          ),
        )
    );
  }


}