import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:whatsapp/all_translations.dart';


class ThirdMapScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new ThirdMapScreenState();
}

class ThirdMapScreenState extends State<ThirdMapScreen> {

  MapController _mapController = new MapController();
  List<Marker> _markers = <Marker> [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }



  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(allTranslations.text("map3"),
          style: TextStyle(
              fontFamily: 'Nazanin',
              fontWeight: FontWeight.w500,
              fontSize: 22
          ),
        ),
      ),
      body: new FlutterMap(
        options: new MapOptions(
            center: new LatLng(38.9275, -77.0202),
            zoom: 15,
            onTap: (LatLng point) {
              setState(() {
                _markers.add(
                    new Marker(
                        width: 40,
                        height: 40,
                        point: point,
                        builder: (context) {
                          return Container(
                            child: new Icon(Icons.location_on, color: Colors.blue,),
                          );
                        }
                    )
                );
              });
            }
          ),
          layers: [
            new TileLayerOptions(
                urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
//                urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?accessToken=${_accessToken}",
                subdomains: ['a', 'b', 'c'],
//                additionalOptions: {
//                  'accessToken': _accessToken,
//                  'id': 'mapbox.streets',
//              }
            ),
            new MarkerLayerOptions(
              markers: _markers
            ),
          ],
          mapController: _mapController,
      ),
    );
  }







}

