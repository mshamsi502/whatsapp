



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/all_translations.dart';

class SignInAnimation extends StatelessWidget {
  final Animation<double> controller;
  final Animation<double> buttonSqueezeAnimation;
  final Animation<double> buttonZoomOut;

  SignInAnimation({this.controller}):

      buttonSqueezeAnimation = new Tween(
        begin: 250.0,
        end: 50.0
      ).animate(
        new CurvedAnimation(
            parent: controller,
            curve: new Interval(0.0, 0.150))
      ),
        buttonZoomOut = new Tween(
            begin: 70.0,
            end: 1000.0
        ).animate(
            new CurvedAnimation(
                parent: controller,
                curve: new Interval(0.550, 0.999, curve: Curves.bounceOut))
        );


  Widget animationBuilder(BuildContext context, Widget child) {
    return buttonZoomOut.value <= 300
        ? new Container(
      margin: const EdgeInsets.only(bottom: 30, top: 30),
      width: buttonZoomOut.value == 70
          ? buttonSqueezeAnimation.value
          : buttonZoomOut.value,
      height: buttonZoomOut.value == 70
          ? 50
          : buttonZoomOut.value,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          gradient: new RadialGradient(
            colors: <Color>[
              const Color(0xff075e54),
              const Color(0x00075e54),
            ],
            center: Alignment.topCenter,
            radius: 2.5,
          ),
          borderRadius: buttonZoomOut.value < 400
              ? new BorderRadius.all(const Radius.circular(30))
              : new BorderRadius.all(const Radius.circular(0))
            ),
          child: buttonSqueezeAnimation.value > 75
            ? new Text(
              allTranslations.text("login_button"),
              style: new TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.w300,
                letterSpacing: 0.3
                ),
              )
            : buttonZoomOut.value < 300
              ? new CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                )
              : null,
          )
        : new Container(
            width: buttonZoomOut.value,
            height: buttonZoomOut.value,
            decoration: new BoxDecoration(
              shape: buttonZoomOut.value <500
                ? BoxShape.circle
                : BoxShape.rectangle,
              color: const Color(0xff075e54)
            ),
        );
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new AnimatedBuilder(
        animation: controller,
        builder: animationBuilder
    );
  }

}