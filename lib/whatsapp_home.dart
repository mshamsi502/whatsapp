import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:whatsapp/all_translations.dart';
import 'package:whatsapp/components/drawer.dart';
import 'package:whatsapp/pages/call_screen.dart';
import 'package:whatsapp/pages/camera_screen.dart';
import 'package:whatsapp/pages/chat_screen.dart';
import 'package:whatsapp/pages/products_screen.dart';



class WhatsAppHome extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new WhatsAppHomeState();

}

class WhatsAppHomeState extends State<WhatsAppHome>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  Map<String, SliverAppBar> appBarList;
  String _currentAppBar = 'mainAppBar';

  @override
  void initState() {
    super.initState();

    tabController = new TabController(initialIndex: 1, length: 4, vsync: this);
    tabController.addListener(() {
      if(tabController.index == 0) {
        tabController.index = tabController.previousIndex;
        Navigator.pushNamed(context, '/camera');
      }
    });

    SliverAppBar mainAppBar = new SliverAppBar(
        title: new Text(allTranslations.text('title')),
        pinned: true,
        floating: true,
        elevation: 5,
        bottom: new TabBar(
          controller: tabController,
          indicatorColor: Colors.white,
          indicatorWeight: 2,
          tabs: <Widget>[
            new Tab(icon: new Icon(Icons.camera_alt)),
            new Tab(text: allTranslations.text('chats')),
            new Tab(text: allTranslations.text('products')),
            new Tab(text: allTranslations.text('calls')),

          ],
        ),
        actions: <Widget>[
          new GestureDetector(
            child: new Icon(Icons.search),
            onTap: () {
              setState(() {
                _currentAppBar = 'searchAppBar';
              });
            },
          ),
          new Padding(
            padding: const EdgeInsets.symmetric(horizontal: 7),
          ),
          new PopupMenuButton<String>( //defultIcon: more_evert
            onSelected: (String choice) async {
              if(choice == 'settings') {
                  Navigator.pushNamed(context, '/settings');
//                  Navigator.push(context, new MaterialPageRoute(builder: (context) => SettingsScreen()));
                }
              else if(choice == 'new_group') {
                Navigator.pushNamed(context, '/new_group');
                }
              else if(choice == 'about') {
                Navigator.pushNamed(context, '/about');
              }
              else if(choice == 'logOut') {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                await prefs.remove('user.api_token');
                Navigator.of(context).pushReplacementNamed('/login');
              }
            },
            itemBuilder: (BuildContext context) {
              return [
                new PopupMenuItem(
                    value: 'new_group',
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Text(allTranslations.text('new_group')),
                      ],
                    )
                ),
                new PopupMenuItem(
                    value: 'settings',
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Text(allTranslations.text('settings')),
                      ],
                    )
                ),
                new PopupMenuItem(
                  value: 'about',
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      new Text(allTranslations.text('about')),
                    ],
                  ),
                ),
                new PopupMenuItem(
                  value: 'logOut',
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      new Text(allTranslations.text('log_out')),
                    ],
                  ),
                ),
              ];
            },
          ),
        ]
    );
    SliverAppBar searchAppBar = new SliverAppBar(
        title: new TextField(
          decoration: new InputDecoration(
            border: InputBorder.none,
            hintText: allTranslations.text('hint_search')
          ),

        ),
        pinned: true,
        //floating: true,
        elevation: 5,
        backgroundColor: Colors.white,
        leading: new GestureDetector(
          child: new Padding(
              padding: const EdgeInsets.only(right: 18.0),
            child: new Icon(Icons.arrow_back, color : new Color(0xff075e54)),
          ),
          onTap:  () {
            setState(() {
              _currentAppBar = 'mainAppBar';
            });
          },
        ),
    );

    appBarList = <String, SliverAppBar> {
      'mainAppBar' : mainAppBar,
      'searchAppBar' : searchAppBar
    };

  }



  Future<bool> _onWillPop() {
    return showDialog(
        context: context,
        builder: (context) {
          return new Directionality(
              textDirection: TextDirection.rtl,
              child: new AlertDialog(
                title: new Text('مطمئنم اشتباهی دستت خورده!',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                ),
                content: new Text('مگه نه؟؟ :))))))',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
              ),
                actions: <Widget>[
                  new FlatButton(
                      onPressed: () => Navigator.of(context).pop(false),
                      child: new Text('آره بابا :)',
                        style: TextStyle(color: Colors.green),
                      )
                  ),
                  new FlatButton(
                      onPressed: () =>exit(0),
                      child: new Text('نه، میخوام برم :|',
                        style: TextStyle(color: Colors.red),
                      )
                  )
                ],
              )
          );
        }
    ) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: new Scaffold(
          drawer: buildDrawerLayout(context),
          body: new NestedScrollView(
              headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  appBarList[_currentAppBar]
                ];
              },
              body: _currentAppBar == 'mainAppBar'
                  ? new TabBarView(
                  controller: tabController,
                  children: <Widget>[
                    new CameraScreen(),
                    new ChatScreen(),
                    new ProductsScreen(),
                    new CallScreen(),
                  ]
              )
                  : new Center(
                child: new Text(allTranslations.text('search')),
              )
          ),
          floatingActionButton: new FloatingActionButton(
              heroTag: 'new_chat',
              backgroundColor: Theme
                  .of(context)
                  .accentColor,
              child: new Icon(Icons.message, color: Colors.white,),
              onPressed: () {
                Navigator.of(context).pushNamed('/new_chat');
//                Navigator.push(
//                  context,
//                  new MaterialPageRoute(
//                      builder: (context) => CreateChatScreen()
//                  ),
//                );
              }
          ),
        ),
        onWillPop: _onWillPop
    );
  }

}
