
import 'package:json_annotation/json_annotation.dart';
import 'package:whatsapp/models/Product.dart';


/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'ProductPageination.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()

class ProductPageination {
  @JsonKey(name: 'current_page')
  int currentPage;
  @JsonKey(name: 'data')
  List<Product> products;
  @JsonKey(name: 'first_page_url')
  String firstPageUrl;
  @JsonKey(name: 'from')
  int from;
  @JsonKey(name: 'last_page')
  int lastPage;
  @JsonKey(name: 'last_page_url')
  String lastPageUrl;
  @JsonKey(name: 'next_page_url')
  String nextPageUrl;
  @JsonKey(name: 'prev_page_url')
  String prevPageUrl;
  @JsonKey(name: 'to')
  int to;
  @JsonKey(name: 'total')
  int total;


  ProductPageination(
      this.currentPage,
      this.products,
      this.firstPageUrl,
      this.from,
      this.lastPage,
      this.lastPageUrl,
      this.nextPageUrl,
      this.prevPageUrl,
      this.to,
      this.total);

  factory ProductPageination.fromJson(Map<String, dynamic> json) => _$ProductPageinationFromJson(json);
  Map<String, dynamic> toJson() => _$ProductPageinationToJson(this);

  @override
  String toString() {
    return 'ProductPageination{currentPage: $currentPage, products: $products, firstPageUrl: $firstPageUrl, from: $from, lastPage: $lastPage, lastPageUrl: $lastPageUrl, nextPageUrl: $nextPageUrl, prevPageUrl: $prevPageUrl, to: $to, total: $total}';
  }


}
