class User {
  final int id;
  final String name;
  final String email;
  final String email_verified_at;
  final String api_token;
  final String created_at;
  final String updated_at;

  User(
      {this.id,
      this.name,
      this.email,
      this.email_verified_at,
      this.api_token,
      this.created_at,
      this.updated_at});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        id: json['id'] as int,
        name: json['name'] as String,
        email: json['email'] as String,
        email_verified_at: json['email_verified_at'] as String,
        api_token: json['api_token'] as String,
        created_at: json['created_at'] as String,
        updated_at: json['updated_at'] as String);
  }

  @override
  String toString() {
    return 'User{id: $id, name: $name, email: $email, email_verified_at: $email_verified_at, api_token: $api_token, created_at: $created_at, updated_at: $updated_at}';
  }
}
