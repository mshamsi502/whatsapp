// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ProductPageination.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductPageination _$ProductPageinationFromJson(Map<String, dynamic> json) {
  return ProductPageination(
    json['current_page'] as int,
    (json['data'] as List)
        ?.map((e) =>
            e == null ? null : Product.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['first_page_url'] as String,
    json['from'] as int,
    json['last_page'] as int,
    json['last_page_url'] as String,
    json['next_page_url'] as String,
    json['prev_page_url'] as String,
    json['to'] as int,
    json['total'] as int,
  );
}

Map<String, dynamic> _$ProductPageinationToJson(ProductPageination instance) =>
    <String, dynamic>{
      'current_page': instance.currentPage,
      'data': instance.products,
      'first_page_url': instance.firstPageUrl,
      'from': instance.from,
      'last_page': instance.lastPage,
      'last_page_url': instance.lastPageUrl,
      'next_page_url': instance.nextPageUrl,
      'prev_page_url': instance.prevPageUrl,
      'to': instance.to,
      'total': instance.total,
    };
